﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    private void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

    }
    //UI Buttons
    public TextMeshProUGUI winnerText;
    public GameObject playAgainText;
    public GameObject playAgainButton;
    public GameObject exitText;
    public GameObject exitButton;

    public void EndGame(string winner)
    {
        if (winner == "Player")
        {
            winnerText.text = "You Win!";
        }
        else
        {
            winnerText.text = "Game Over!";
        }
        winnerText.gameObject.SetActive(true);
        playAgainButton.SetActive(true);
        exitButton.SetActive(true);
        playAgainText.SetActive(true);
        exitText.SetActive(true);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }
    public void Exit()
    {
        SceneManager.LoadScene(0);
    }
    
}
