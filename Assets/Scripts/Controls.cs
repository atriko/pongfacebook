﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    public static bool CanPlay = false;
    protected float paddleSpeed = 3.6f; // paddle speed both for computer and player
    protected Vector3 startPosition;
    void Awake()
    {
        startPosition = transform.position;
    }
    public void ResetPosition()
    {
        transform.position = startPosition;
    }

    protected void CheckBoundaries() // prevent going out of bounds for player and computer
    {
        if (transform.position.x >= 2f)
        {
            transform.position = new Vector3(2f, transform.position.y);
        }
        else if (transform.position.x <= -2f)
        {
            transform.position = new Vector3(-2f, transform.position.y);
        }
    }
}
