﻿using System.Collections;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    //oppenent base to call when they got a score
    public GameObject oppenentBase;

    //paddles and ball for resetting their positions after each round
    public GameObject playerPaddle;
    public GameObject computerPaddle;
    public GameObject ball;

    //score text to update
    public TextMeshProUGUI scoreText;


    private int score = 0;
    private int winningScore = 5; // winning score default is 5
    private float roundBreak = 1.5f; // wait x seconds before the next round starts

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            ball.SetActive(false);

            StartCoroutine(Scored());
        }
    }

    private IEnumerator Scored()
    {
        yield return new WaitForSeconds(0.5f);
        oppenentBase.GetComponent<ScoreManager>().GetScore();
    }

    private void GetScore()
    {
        score++;
        scoreText.text = score.ToString();
        if (score == winningScore) // if you have reached to winning score its game over
        {
            GameManager.Instance.EndGame(gameObject.tag);
        }
        else // if not you should reset positions of everything and start next round
        {
            oppenentBase.GetComponent<ScoreManager>().ResetPositions();
        }
    }

    private void ResetPositions() // reset positions of ball computer player and start new round
    {
        Controls.CanPlay = false;
        computerPaddle.GetComponent<ComputerScript>().ResetPosition();
        playerPaddle.GetComponent<PlayerControls>().ResetPosition();
        ball.transform.position = new Vector2(0, 0);

        StartCoroutine(StartNewRound());
    }

    private IEnumerator StartNewRound() // start new round after a break
    {
        ball.SetActive(true);
        yield return new WaitForSeconds(roundBreak);
        Controls.CanPlay = true;
        ball.GetComponent<Ball>().StartGame();
    }
    
}
