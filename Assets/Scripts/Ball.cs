﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody2D rb;
    private float ballSpeed = 320f; // ball speed
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    public void StartGame()
    {
        Controls.CanPlay = true;
        float randomDirection = Random.Range(75f, 200f); // give random direction to the ball at the start of the round
        rb.AddForce(new Vector2 (randomDirection, ballSpeed));
    }
}
