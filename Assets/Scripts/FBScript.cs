﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FBScript : MonoBehaviour
{
    public static string userName, userLastname;
    public static Texture2D userProfilePicture;

    public GameObject mainMenuPlayButton, mainMenuLoginButton, mainMenuLoginOutButton, loadingCircle, mainMenuProfilePicture;
    public TextMeshProUGUI mainMenuText;

    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(SetInit,OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void SetInit()
    {
        if (FB.IsInitialized)
            FB.ActivateApp();
        else
            Debug.Log("Failed to Initialize the Facebook SDK");
    }

    private void OnHideUnity(bool isUnityShown)
    {
        if (!isUnityShown)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void FacebookLogin()
    {
        var perms = new List<string>() { "public_profile" };
        FB.LogInWithReadPermissions(perms,LoginCallback);
    }
    public void FacebookLogout()
    {
        FB.LogOut();
        SceneManager.LoadScene(0);
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    private void LoginCallback(ILoginResult result)
    {
        if (result.Error != null)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                FB.API("/me?fields=first_name", HttpMethod.GET, UpdateUsername);
                FB.API("/me?fields=last_name", HttpMethod.GET, UpdateLastname);
                FB.API("/me/picture?width=200&height=200", HttpMethod.GET, UpdateProfilePicture);
                
                StartCoroutine(WaitForInfoLoad());

            }
            else
            {
                Debug.Log("Not Logged in");
            }
        }
    }

    private IEnumerator WaitForInfoLoad()
    {
        LoggingIn();
        yield return new WaitUntil(() => userProfilePicture != null);
        mainMenuProfilePicture.GetComponent<SpriteRenderer>().sprite = Sprite.Create(userProfilePicture, new Rect(0, 0, 200, 200), new Vector2(0.5f, 0.5f));
        yield return new WaitUntil(() => userName != null);
        mainMenuText.text = userName + ", you are ready to play!";
        LoggedIn();
    }

    private void LoggedIn()
    {
        loadingCircle.SetActive(false);
        mainMenuPlayButton.SetActive(true);
        mainMenuLoginOutButton.SetActive(true);
    }

    private void LoggingIn()
    {
        mainMenuLoginButton.SetActive(false);
        loadingCircle.SetActive(true);
    }

    private void UpdateUsername(IGraphResult result)
    {
        if (result.Error == null)
        {
            userName = result.ResultDictionary["first_name"].ToString();
        }
    }
    private void UpdateLastname(IGraphResult result)
    {
        if (result.Error == null)
        {
            userLastname = result.ResultDictionary["last_name"].ToString();
        }
    }

    private void UpdateProfilePicture(IGraphResult result)
    {
        if (result.Error == null)
        {
            userProfilePicture = result.Texture;
        }
    }
    
}
