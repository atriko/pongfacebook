﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : Controls
{
    void Update()
    {
        if (Input.GetMouseButton(0) && CanPlay)
        {
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            int direction = (touchPosition.x > transform.position.x) ? 1 : -1; //get your direction from which side of the object you touched
            MovePlayer(direction);
        }
    }
    private void MovePlayer(int direction)
    {
        CheckBoundaries();
        transform.position += new Vector3(direction * Time.deltaTime * paddleSpeed, 0);

    }
}
