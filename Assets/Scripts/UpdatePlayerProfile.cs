﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdatePlayerProfile : MonoBehaviour
{
    public TextMeshProUGUI userName;
    public SpriteRenderer userPicture;
    private void Start()
    {
        userPicture.sprite = Sprite.Create(FBScript.userProfilePicture, new Rect(0, 0, 200, 200), new Vector2(0.5f, 0.5f));
        userName.text = FBScript.userName + " " + FBScript.userLastname;
    }
}
