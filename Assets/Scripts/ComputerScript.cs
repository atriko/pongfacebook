﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerScript : Controls
{
    public Transform ball;
    private float computerAccuracy = 0.5f; // higher means worse zero is exactly as ball
    void Update()
    {
        CheckBoundaries();
        if (CanPlay)
        {
            if (ball.position.x - transform.position.x < -computerAccuracy) //choose your direction on where the ball is
            {
                MoveComputer(-1);
            }
            else if (ball.position.x - transform.position.x > computerAccuracy)
            {
                MoveComputer(1);
            }
        }
        
    }

    private void MoveComputer(int direction) // move towards ball
    {
        transform.position += new Vector3(direction * Time.deltaTime * paddleSpeed, 0);
    }

}
